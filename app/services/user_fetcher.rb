class UserFetcher
  prepend SimpleCommand

  attr_reader :mobile, :last_name, :first_name

  def initialize(mobile: nil, last_name: nil, first_name: nil)
    @mobile = mobile
    @last_name = last_name
    @first_name = first_name

    raise InvalidNilParameter, instance_values if instance_values.values.include?(nil) || instance_values.values.include?('')
  end

  def call
    User.find_by(mobile: mobile, last_name: last_name, first_name:first_name)
  end


end