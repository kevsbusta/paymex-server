class CheckoutsController < ApplicationController
  def new
    client_token = BraintreeTokenGenerator.call()
    render json: { client_token: client_token }, status: :accepted
  end
end