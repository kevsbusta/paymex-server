# Displays error with message
class ErrorWithMessage < StandardError
  attr_reader :message

  def initialize(message)
    super
    @message = message
  end
end
