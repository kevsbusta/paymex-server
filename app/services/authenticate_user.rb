# app/commands/authenticate_user.rb
class AuthenticateUser
  prepend SimpleCommand
  attr_reader :mobile, :password

  def initialize(mobile, password)
    @mobile = mobile
    @password = password
  end

  def call
    JsonWebToken.encode(
      user_id: @user.id,
      first_name: @user.first_name,
      last_name: @user.last_name
      ) if is_user
  end

  private

  def is_user
    @user = User.
              where(mobile: mobile, password: password).
              first
    return @user if @user

    errors.add :user_authentication, 'invalid credentials'
    nil
  end
end
