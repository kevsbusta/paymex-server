class PaymexKeySerializer < ActiveModel::Serializer
  attributes :id, :receiver, :days_remaining, :description, :granted_amount, :currency, :created_at

  def receiver
    object.receiver.try(:full_name)
  end

  def sender
    object.sender.try(:full_name)
  end

  def days_remaining
    object.expires_at.mjd - Time.now.to_date.mjd
  end

  def currency
    "PHP"
  end

  def granted_amount
    '%.2f' % object.granted_amount
  end

  def created_at
    object.created_at.to_date.to_formatted_s(:long)
  end

end
