# app/commands/authenticate_merchant.rb
class AuthenticateMerchant
  prepend SimpleCommand
  attr_reader :email, :password

  def initialize(email, password)
    @email = email
    @password = password
  end

  def call
    JsonWebToken.encode(
      user_id: @user.id,
      merchant_id: @user.merchant_id,
      first_name: @user.first_name,
      last_name: @user.last_name
      ) if is_merchant
  end

  private

  def is_merchant
    @user = User.
              where.not(merchant_id: nil).
              where(email: email, password: password).
              first
    return @user if @user

    errors.add :merchant_authentication, 'invalid credentials'
    nil
  end
end
