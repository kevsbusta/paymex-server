class PaymexKeysController < UserAuthApiController

  before_action :verify_action_allowed, only: [:cancel, :approve, :deny]

  def verify_action_allowed
    @paymex_key = paymex_key
    raise ActionNotAllowed unless user_id.in?([@paymex_key.receiver_id, @paymex_key.sender_id])
  end

  def cancel
    render json: @paymex_key, status: :ok if @paymex_key.cancel!
  end

  def approve
    render json: @paymex_key, status: :ok if @paymex_key.approve!
  end

  def deny
    render json: @paymex_key, status: :ok if @paymex_key.deny!
  end

  private

  def paymex_key
    PaymexKey.find_by(id: params[:id])
  end
end
