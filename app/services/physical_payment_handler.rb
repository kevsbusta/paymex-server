class PhysicalPaymentHandler
  prepend SimpleCommand

  attr_reader :amount, :description, :expiry_date, :key, :merchant_id

  def initialize(physical_merchant_payments_params: {}, merchant_id: nil)
    @amount = physical_merchant_payments_params[:amount].try(:strip)
    @description = physical_merchant_payments_params[:description].try(:strip)
    @expiry_date = physical_merchant_payments_params[:expiry_date].try(:strip)
    @key = physical_merchant_payments_params[:key].try(:strip)
    @merchant_id = merchant_id


    raise InvalidNilParameter, instance_values if instance_values.values.include?(nil) || instance_values.values.include?('')
  end


  def call
    paymex_key = retrieve_paymex_key
    merchant_transaction  =  retrieve_merchant_transaction

    payment_processor =  PaymentProcessor.call(paymex_key: paymex_key, merchant_transaction: merchant_transaction)

    return merchant_transaction.reference if payment_processor.success?
    nil
  end


  def retrieve_paymex_key
    paymex_key_fetcher_command = PaymexKeyFetcher.call(
        key: key,
        expiry_date: expiry_date
    )

    return paymex_key_fetcher_command.result if paymex_key_fetcher_command.success?
    nil
  end

  def retrieve_merchant_transaction
    merchant_transaction_key_fetcher_command = MerchantTransactionCreator.call(merchant_transaction_params: {amount:amount, description: description}, merchant_id: merchant_id)
    return merchant_transaction_key_fetcher_command.result if merchant_transaction_key_fetcher_command.success?
    nil
  end

end