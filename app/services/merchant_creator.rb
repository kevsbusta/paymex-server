class MerchantCreator
  prepend SimpleCommand

  attr_reader :merchant_params

  def initialize(merchant_params: {})
    @merchant_params = merchant_params
  end

  def call
    @merchant = create_merchant_and_generate_code

    if @merchant
      return @merchant
    end
    nil
  end

  def create_merchant_and_generate_code
    Merchant.transaction do
      @merchant = Merchant.create!(merchant_params)
      merchant_code_command = RandomTokenGenerator.call(uniq_id: @merchant.id)
      @merchant.update!(code: merchant_code_command.result)
    end

    @merchant
  end
end