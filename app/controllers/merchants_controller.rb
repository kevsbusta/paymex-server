class MerchantsController < ApplicationController
  def index
    @merchants = Merchant.all
    render json: @merchants, status: :ok
  end

  def create
    command = MerchantCreator.call(merchant_params: merchant_params)
    render json: command.result, status: :ok if command.success?
  end

  def show
    @merchant = Merchant.find_by(id: (params[:id]))
    render json: @merchant, status: :ok
  end

  def update
    @merchant = Merchant.find_by(id: (params[:id]))
    @merchant = Merchant.update!(merchant_params)
    render json: @merchant, status: :ok
  end

  def merchant_params
    params.require(:merchant).permit(:name)
  end
end
