# app/services/authorize_merchant_api_request.rb
class AuthorizeMerchantApiRequest < AuthorizeApiRequest
  prepend SimpleCommand

  def initialize(headers = {})
    @headers = headers
  end

  def call
    merchant_user
  end

  private

  def merchant_user
    user = User.
        where.
        not(merchant_id: nil).
        where(
          id: auth_token[:user_id],
          merchant_id: auth_token[:merchant_id]
      ).first if auth_token

    if user
      return user
    else
      errors.add(:token, 'Invalid token')
      return false
    end

  end
end
