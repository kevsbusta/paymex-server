class CreateMerchants < ActiveRecord::Migration[5.0]
  def change
    create_table :merchants do |t|
      t.string :name
      t.string :reference
      t.string :status, default: 'active'
      t.timestamps
    end
  end
end
