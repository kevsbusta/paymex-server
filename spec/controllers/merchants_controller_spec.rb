require 'rails_helper'

RSpec.describe MerchantsController, type: :controller do
  include_context 'authenticate_user'


  describe 'POST create' do
    subject(:context) do
      post :create, params: {merchant: {name: name}}
    end

    context 'when create is successful' do
      let(:name) { 'SomeMerchantName' }

      it 'creates merchant' do
        expect{ context }.to change(Merchant, :count).by(1)
      end
      
      it 'generates merchant code' do
        actual_result = JSON.parse(context.body)["code"]
        expect(actual_result).to eq Merchant.last.code
      end
    end
    
    context 'when name is not present' do
      let(:name) { nil }
      it 'does not create merchant' do
        expect{ context }.to change(Merchant, :count).by(0)
      end

      it 'returns error' do
        actual_result = JSON.parse(context.body)
        expected_result = {'message'=>"Validation failed: Name can't be blank"}
        expect(actual_result).to eq expected_result
      end
    end
  end

end
