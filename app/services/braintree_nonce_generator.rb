class BraintreeNonceGenerator
  prepend SimpleCommand

  attr_reader :payment_method_token

  def initialize(payment_method_token: nil)
    @payment_method_token = payment_method_token

    raise InvalidNilParameter, instance_values if instance_values.values.include?(nil)
  end

  def call
    Braintree::PaymentMethodNonce.create(payment_method_token)
  end

end
