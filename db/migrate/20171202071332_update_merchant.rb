class UpdateMerchant < ActiveRecord::Migration[5.0]
  def change
    rename_column :merchants, :reference, :code
  end
end
