class CreateMerchantTransactions < ActiveRecord::Migration[5.0]
  def change
    create_table :merchant_transactions do |t|
      t.integer :merchant_id
      t.decimal :amount
      t.string :description
      t.string :reference
      t.string  :status, :default => "pending"
      t.integer :paymex_key_id
      t.datetime :paid_at
      t.string :status_last_updated_at


      t.timestamps
    end
  end
end
