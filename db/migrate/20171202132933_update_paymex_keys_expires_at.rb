class UpdatePaymexKeysExpiresAt < ActiveRecord::Migration[5.0]
  def change
    change_column :paymex_keys, :expires_at, :date
  end
end
