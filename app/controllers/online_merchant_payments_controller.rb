class OnlineMerchantPaymentsController < ApplicationController

  def create
    online_payment_handler = OnlinePaymentHandler.call(
        online_merchant_payments_params: online_merchant_payments_params,
    )

    if online_payment_handler.success?
  		render json: online_payment_handler.result, status: :ok
  	else
  		render json: "Transaction cannot be completed.", status: :precondition_failed
  	end
  end

  def online_merchant_payments_params
    params.permit(:reference, :key, :expiry_date)
  end
end
