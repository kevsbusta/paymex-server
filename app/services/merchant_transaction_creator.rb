class MerchantTransactionCreator
  prepend SimpleCommand

  attr_reader :merchant_transaction_params, :merchant_id

  def initialize(merchant_transaction_params: {}, merchant_id: nil)
    @merchant_id = merchant_id
    @merchant_transaction_params = merchant_transaction_params
  end

  def call
    @merchant_transaction = create_merchant_transaction_and_generate_reference

    if @merchant_transaction
      return @merchant_transaction
    end
    nil
  end

  def create_merchant_transaction_and_generate_reference
    MerchantTransaction.transaction do
      @merchant_transaction = MerchantTransaction.create!(merchant_transaction_params.merge(merchant_id: merchant_id))
      random_token_command = RandomTokenGenerator.call(uniq_id: @merchant_transaction.id, type: :merchant_transaction_reference_token)
      @merchant_transaction.update!(reference: random_token_command.result)
    end

    @merchant_transaction
  end
end