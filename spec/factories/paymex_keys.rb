FactoryBot.define do
  sequence :key do |n|
    RandomTokenGenerator.call(uniq_id: n, type: :merchant_code_token).result
  end

  factory :paymex_key do
    key
    sequence(:receiver_id) { create(:user).id }
    sequence(:sender_id) { create(:user).id }
  end

  factory :approved_paymex_key, class: PaymexKey do
    key
    status 'approved'
    sequence(:receiver_id) { create(:user).id }
    sequence(:sender_id) { create(:user).id }
  end

  factory :pending_paymex_key, class: PaymexKey do
    key
    status 'pending'
    sequence(:receiver_id) { create(:user).id }
    sequence(:sender_id) { create(:user).id }
  end
end

