FactoryBot.define do
  sequence :email do |n|
    "#{Faker::Internet.email}#{n}"
  end

  factory :user, class: 'User' do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    mobile { Faker::PhoneNumber.phone_number }
    password 'foobar123'
    email
    birth_date { Faker::Date.birthday(min_age = 18, max_age = 65) }


    factory :merchant_user, class: 'User' do
      after(:create) do |user, _evaluator|
        user.merchant_id = create(:merchant).id
        user.save
      end
    end

  end
end
