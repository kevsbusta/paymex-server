# app/commands/braintree_token_generator.rb
class BraintreeTokenGenerator
  prepend SimpleCommand

  def initialize()
  end

  def call
    Braintree::ClientToken.generate
  end

  private
end
