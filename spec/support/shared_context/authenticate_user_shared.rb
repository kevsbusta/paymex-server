shared_examples 'authenticate_user' do
  let(:user) { create(:user) }

  before do
    authenticate
  end

  def authenticate
    token = JsonWebToken.encode(user_id: user.id)
    request.headers['Authorization'] = token
  end
end
