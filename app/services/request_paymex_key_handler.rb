class RequestPaymexKeyHandler
  prepend SimpleCommand

  attr_reader :request_key_params, :user_id
  def initialize(request_key_params: nil, user_id: nil)
    @request_key_params = request_key_params
    @user_id = user_id

    raise InvalidNilParameter, instance_values if instance_values.values.include?('') ||  instance_values.values.include?(nil)
  end

  def call

      PaymexKey.create!(
          granted_amount: request_key_params["granted_amount"],
          description: request_key_params["description"],
          sender_id: sender_id,
          receiver_id: user_id,
          status: "for_approval"
      )

  end

  def sender_id
    user_fetcher_command = UserFetcher.call(
        mobile: request_key_params[:mobile],
        first_name: request_key_params[:first_name],
        last_name: request_key_params[:last_name]
    )

    return user_fetcher_command.result.id if user_fetcher_command.success?
    nil
  end

end