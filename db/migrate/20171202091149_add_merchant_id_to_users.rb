class AddMerchantIdToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :merchant_id, :integer
  end
end
