class PhysicalMerchantPaymentsController < ApplicationController

  def create
    physical_payment_handler = PhysicalPaymentHandler.call(
        physical_merchant_payments_params: physical_merchant_payments_params,
        merchant_id: merchant_id
    )
    render json: physical_payment_handler.result, status: :ok if physical_payment_handler
  end

  # TODO: update merchant_id
  def merchant_id
    1
  end

  def physical_merchant_payments_params
    params.permit(:description, :amount, :key, :expiry_date)
  end
end
