class ReceivedPaymexKeysController < UserAuthApiController
  def index
    @paymex_keys = PaymexKey.where(receiver_id: user_id)
    render json: @paymex_keys, status: :ok, root: 'data'
  end
end
