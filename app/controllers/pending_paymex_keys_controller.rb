class PendingPaymexKeysController < UserAuthApiController
  def index
    @paymex_keys = PaymexKey.where(sender_id: user_id, status: "for_approval")
    render json: @paymex_keys, status: :ok, root: 'data'
  end
end
