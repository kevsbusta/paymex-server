class SendPaymexKeysController < UserAuthApiController
  def create
    command = SendPaymexKeyHandler.call(send_key_params: send_key_params, user_id: user_id)
    render json: command.result, status: :ok if command.success?
  end

  def send_key_params
    params.permit(:mobile, :granted_amount, :description)
  end
end
