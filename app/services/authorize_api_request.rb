# app/services/authorize_api_request.rb
class AuthorizeApiRequest
  attr_reader :headers

  private

  def auth_token
    @decoded_auth_token ||= JsonWebToken.decode(http_auth_header)
  end

  def http_auth_header
    return parsed_token if headers['Authorization'].present?
    errors.add(:token, 'Missing token')
    nil
  end

  def parsed_token
    headers['Authorization'].split(' ').last
  end
end
