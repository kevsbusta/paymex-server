require 'rails_helper'

RSpec.describe MerchantAuthenticationController, type: :controller do
	let!(:hashed_password) { Digest::MD5.hexdigest('foobar') }
  let!(:merchant) { create(:merchant, name: 'Lazada') }
  let!(:user) { create(:user, email: 'test@domain.com', password: hashed_password, merchant_id: merchant.id) }


 	subject(:context) do
  	post :authenticate, params:
    	{
    		email: email,
    		password: password
    	}
	end

  describe '.authenticate' do

    context 'when user is authorized' do
    	let!(:email) { "test@domain.com" }
    	let!(:password) { hashed_password }

      it 'returns status 200' do
        expect(context).to have_http_status(200)
      end

      it 'returns a valid token in the response' do
        actual = JSON.parse(context.body)
        expect(actual['auth_token']).to_not be_nil
      end

      it 'returns a valid token with claims of id' do
        res = JSON.parse(context.body)
        secret_key = Rails.application.secrets.secret_key_base
        actual = JWT.decode(res['auth_token'], secret_key).first

        expect(actual['user_id']).to eq(user.id)
      end
    end

    context 'when user is NOT authorized' do
    	context 'when user has an invalid email' do
    		let(:email) { 'test@gmail.com' }
				let(:password) { 'IamNotCorrect' }

	      it 'returns status 401' do
	        expect(context).to have_http_status(401)
	      end
    	end

      context 'whene password is incorrect' do
	 			let(:email) { 'test@gmail.com' }
	 			let(:password) { 'IamNotCorrect' }

	 			it 'returns status 401' do
        	expect(context).to have_http_status(401)
      	end
      end

      context 'if user is not merchant' do
	 			let(:email) { 'test@gmail.com' }
	 			let(:password) { 'IamNotCorrect' }

	 			it 'returns status 401' do
	      	user.update(merchant_id: nil)
	        expect(context).to have_http_status(401)
      	end
      end
    end
  end
end
