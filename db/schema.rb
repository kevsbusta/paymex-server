# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171202200359) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "merchant_transactions", force: :cascade do |t|
    t.integer  "merchant_id"
    t.decimal  "amount"
    t.string   "description"
    t.string   "reference"
    t.string   "status",                 default: "pending"
    t.integer  "paymex_key_id"
    t.datetime "paid_at"
    t.string   "status_last_updated_at"
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  create_table "merchants", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.string   "status",     default: "active"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "paymex_keys", force: :cascade do |t|
    t.string   "description"
    t.string   "key"
    t.string   "currency"
    t.decimal  "granted_amount"
    t.integer  "receiver_id"
    t.integer  "sender_id"
    t.string   "status",                 default: "pending"
    t.datetime "status_last_updated_at"
    t.date     "expires_at"
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "mobile"
    t.string   "password"
    t.string   "email"
    t.string   "status",       default: "active"
    t.date     "birth_date"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "merchant_id"
    t.string   "braintree_id"
  end

end
