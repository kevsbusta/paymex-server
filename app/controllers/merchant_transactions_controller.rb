class MerchantTransactionsController < ApplicationController
  def create
    raise MerchantNotFound unless merchant

    merchant_transaction = MerchantTransactionCreator.call(merchant_transaction_params: merchant_transaction_params, merchant_id: merchant.id)
    render json: { reference: merchant_transaction.result.reference }, status: :ok if merchant_transaction.success?
  end

  def show
    merchant_transaction = MerchantTransaction.find_by(reference: reference)
    result = {
        reference: merchant_transaction.reference,
        amount: merchant_transaction.amount,
        description: merchant_transaction.description,
        merchant: Merchant.find_by(id: merchant_transaction.merchant_id ).try(:name)
    }
    render json: result, status: :ok
  end

  private

  def reference
    params[:id]
  end

  def merchant
    Merchant.find_by(code: params[:merchant_code])
  end

  def merchant_transaction_params
    params.permit(:amount, :description)
  end
end
