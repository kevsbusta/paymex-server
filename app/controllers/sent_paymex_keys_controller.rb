class SentPaymexKeysController < ApplicationController
  def index
    @paymex_keys = PaymexKey.where(sender_id: user_id)
    render json: @paymex_keys, status: :ok, root: 'data'
  end
end
