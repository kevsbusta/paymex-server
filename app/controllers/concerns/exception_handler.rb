# Handles the exception of the controller
module ExceptionHandler
  extend ActiveSupport::Concern

  included do
    rescue_from ActiveRecord::RecordNotFound do |e|
      render json: { message: e.message }, status: :not_found
    end

    rescue_from ActiveRecord::RecordInvalid do |e|
      render json: { message: e.message }, status: :unprocessable_entity
    end

    rescue_from JsonWebToken::InvalidToken do |e|
      render json: { message: e.message }, status: :unauthorized
    end

    rescue_from AmountInsufficient do |e|
      render json: { message: 'Paymex key has insufficient credit' }, status: :unprocessable_entity
    end

    rescue_from InvalidNilParameter do
      render json: { message: 'Parameters provided contains or empty value' }, status: :unprocessable_entity
    end

    rescue_from PaymexKeyExpired do
      render json: { message: 'Paymex key has expired' }, status: :unprocessable_entity
    end

    rescue_from PaymexKeyInvalid do
      render json: { message: 'Paymex key invalid' }, status: :unprocessable_entity
    end

    rescue_from MerchantNotFound do
      render json: { message: 'Merchant not found' }, status: :unprocessable_entity
    end

    rescue_from PaymexKeyNotApproved do
      render json: { message: 'Paymex key invalid' }, status: :unprocessable_entity
    end

    rescue_from Braintree::AuthenticationError do
      render json: { message: "Cannot access Payment Portal" }, status: :service_unavailable
    end

    rescue_from Braintree::AuthorizationError do
      render json: { message: "Cannot access Payment Portal" }, status: :service_unavailable
    end

    rescue_from Braintree::UnexpectedError do
      render json: { message: "Cannot access Payment Portal" }, status: :service_unavailable
    end

    rescue_from ActionNotAllowed do
      render json: { message:  "User not allowed to perform action" }, status: :unprocessable_entity
    end

  end
end
