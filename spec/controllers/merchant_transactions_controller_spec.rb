require 'rails_helper'

RSpec.describe MerchantTransactionsController, type: :controller do
  include_context 'authenticate_user'

  describe 'POST create' do
    subject(:context) do
      post :create,
           params: {
               amount: amount,
               description: description,
               merchant_code: merchant_code,
           }
    end

    context 'when create is successful' do
      let(:merchant) { create(:merchant) }
      let(:merchant_code) { merchant.code }
      let(:description) { Faker::Commerce.product_name }
      let(:amount) { Faker::Commerce.price }

      before do
        allow(BraintreeTokenGenerator).to receive(:call) { "token"}
      end

      it 'creates merchant transaction' do
        expect{ context }.to change(MerchantTransaction, :count).by(1)
      end

      it 'generates merchant reference' do
        actual_result = JSON.parse(context.body)["reference"]
        expect(actual_result).to eq MerchantTransaction.last.reference
      end

      it 'returns accepted' do
        expect(context).to have_http_status(:ok)
      end
    end

    context 'when amount is not present' do
      let(:merchant) { create(:merchant) }
      let(:merchant_code) { merchant.code }
      let(:description) { Faker::Commerce.product_name }
      let(:amount) { nil }
      it 'does not create merchant transaction' do
        expect{ context }.to change(MerchantTransaction, :count).by(0)
      end

      it 'returns error' do
        actual_result = JSON.parse(context.body)
        expected_result = {'message'=>"Validation failed: Amount can't be blank"}
        expect(actual_result).to eq expected_result
      end
    end

    context 'when description is not present' do
      let(:merchant) { create(:merchant) }
      let(:merchant_code) { merchant.code }
      let(:description) { nil }
      let(:amount) { Faker::Commerce.price }
      it 'does not create merchant transaction' do
        expect{ context }.to change(MerchantTransaction, :count).by(0)
      end

      it 'returns error' do
        actual_result = JSON.parse(context.body)
        expected_result = {'message'=>"Validation failed: Description can't be blank"}
        expect(actual_result).to eq expected_result
      end
    end

    context 'when description is not present' do
      let(:merchant) { create(:merchant) }
      let(:merchant_code) { "ABCDE" }
      let(:description) { nil }
      let(:amount) { Faker::Commerce.price }
      it 'raises an error' do
        actual_result = JSON.parse(context.body)
        expected_result = {"message"=>"Merchant not found"}
        expect(actual_result).to eq(expected_result)
      end
    end
  end

end
