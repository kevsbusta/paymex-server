# app/services/authorize_user_api_request.rb
class AuthorizeUserApiRequest < AuthorizeApiRequest
  prepend SimpleCommand

  def initialize(headers = {})
    @headers = headers
  end

  def call
    user
  end

  private

  def user
    user = User.where(
      id: auth_token[:user_id],
      merchant_id: nil
    ).first if auth_token

    if user
      return user
    else
      errors.add(:token, 'Invalid token')
      return false
    end

  end
end
