class OnlinePaymentHandler
  prepend SimpleCommand

  attr_reader :mt_reference, :expiry_date, :key

  def initialize(online_merchant_payments_params: {}, merchant_id: nil)
    @expiry_date = online_merchant_payments_params[:expiry_date].try(:strip)
    @key = online_merchant_payments_params[:key].try(:strip)
    @mt_reference = online_merchant_payments_params[:reference].try(:strip)

    raise InvalidNilParameter, instance_values if instance_values.values.include?(nil) || instance_values.values.include?('')
  end


  def call
    paymex_key = retrieve_paymex_key
    merchant_transaction  =  retrieve_merchant_transaction

    braintree_processor = BraintreeProcessor.call(
        paymex_key: paymex_key,
        merchant_transaction: merchant_transaction
     )

    if braintree_processor.success?
      payment_processor = PaymentProcessor.call(
          paymex_key: paymex_key,
          merchant_transaction: merchant_transaction
        )
    else
      errors.add :braintree, 'Cannot complete transtion to braintree'
    end

    return merchant_transaction.reference if payment_processor && payment_processor.success?
    nil
  end


  def retrieve_paymex_key
    paymex_key_fetcher_command = PaymexKeyFetcher.call(
        key: key,
        expiry_date: expiry_date
    )

    return paymex_key_fetcher_command.result if paymex_key_fetcher_command.success?
    nil
  end

  def retrieve_merchant_transaction
    MerchantTransaction.find_by(reference: mt_reference)
  end
end