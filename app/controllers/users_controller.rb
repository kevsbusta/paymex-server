class UsersController < UserAuthApiController

  skip_before_action :authenticate_user, only: [:create]

  def create
    user = User.create!(user_params.merge(birth_date: birth_date))
    render json: user, status: :ok
  end

  def show
    user = User.find_by(id: (params[:id]))
    render json: user, status: :ok
  end

  private

  def birth_date
    Date.parse(params[:birth_date], '%Y-%m-%d')
  end

  def user_params
    params.permit(:first_name, :last_name, :mobile, :email, :password)
  end
end
