module ClaimHandler
  extend ActiveSupport::Concern

  private

  def merchant_id
    JsonWebToken.decode(request.headers['Authorization'])[:merchant_id]
  end

  def user_id
    JsonWebToken.decode(request.headers['Authorization'])[:user_id]
  end
end
