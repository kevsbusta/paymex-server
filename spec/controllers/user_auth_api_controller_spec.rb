require 'rails_helper'

RSpec.describe UserAuthApiController, type: :controller do

  describe '.authenticate_request' do

    controller(UserAuthApiController) do
      before_action :authenticate_user
      def custom
        render json: { message: 'success!' }
      end
    end

    before do
      routes.draw { get 'custom' => 'user_auth_api#custom' }
    end

    subject(:context) do
      get 'custom'
    end

    context 'when request has NO Authorization headers' do
      it 'should respond with status 401' do
        expect(context).to have_http_status(401)
      end
    end

    context 'when request has Authorization headers' do
		  include_context 'authenticate_user'

      it 'should respond with status 200' do
        expect(context).to have_http_status(200)
      end
    end

    context 'when request token is a merchant' do
		  include_context 'authenticate_merchant'

      it 'should respond with status 401' do
        expect(context).to have_http_status(401)
      end
    end
  end
end
