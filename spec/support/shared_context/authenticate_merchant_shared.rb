shared_examples 'authenticate_merchant' do
  let(:merchant_user) { create(:merchant_user) }

  before do
    authenticate
  end

  def authenticate
    token = JsonWebToken.encode(
    	user_id: merchant_user.id,
    	merchant_id: merchant_user.merchant_id
  	)
    request.headers['Authorization'] = token
  end
end
