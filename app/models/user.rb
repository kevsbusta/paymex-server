# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  first_name :string
#  last_name  :string
#  mobile     :string
#  password   :string
#  email      :string
#  status     :string           default("active")
#  birth_date :date
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class User < ApplicationRecord
  validates :first_name, :last_name, :mobile, :password, :status, :birth_date, presence: true
  validates :mobile, :email, uniqueness: true

  def full_name
    "#{first_name} #{last_name}"
  end
end
