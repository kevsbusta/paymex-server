# lib/json_web_token.rb
class JsonWebToken
  InvalidToken = Class.new(StandardError)

  class << self
    def encode(payload, expiration = 60.minutes.from_now)
      return unless payload.is_a? Hash

      payload[:exp] = expiration.to_i
      JWT.encode(payload, secret_key_base)
    end

    def decode(token)
      body = JWT.decode(token, secret_key_base).try(:first)
      body.try(:symbolize_keys!)
    rescue JWT::DecodeError
      raise InvalidToken, 'Token is invalid or already expired'
    end

    def secret_key_base
      Rails.application.secrets.secret_key_base
    end
  end
end
