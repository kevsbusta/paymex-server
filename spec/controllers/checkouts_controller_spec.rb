require 'rails_helper'

RSpec.describe CheckoutsController, type: :controller do

  describe 'POST create' do
    let(:merchant) { create(:merchant, id: 1) }

    subject(:context) do
      post :new
    end

    context 'when getting new token is succesful' do
      it 'generates client_token' do
        allow(BraintreeTokenGenerator).to receive(:call) { 'token' }
        actual_result = JSON.parse(context.body)["client_token"]
        expect(actual_result).to_not be_nil
      end
    end

    context 'when merchant_id is not recognized by braintree' do
      it 'returns service_unavailable' do
        allow(Braintree::Configuration)
          .to receive(:merchant_id) { 'invalid merchant id' }
        expect(context).to have_http_status(:service_unavailable)
      end
    end

    context 'when private key is not recognized by braintree' do
      it 'returns service_unavailable' do
        allow(Braintree::Configuration)
          .to receive(:private_key) { 'invalid private_key' }
        expect(context).to have_http_status(:service_unavailable)
      end
    end

    context 'when public key is not recognized by braintree' do
      it 'returns service_unavailable' do
        allow(Braintree::Configuration)
          .to receive(:public_key) { 'invalid public_key' }
        expect(context).to have_http_status(:service_unavailable)
      end
    end
  end
end
