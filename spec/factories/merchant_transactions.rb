FactoryBot.define do
  sequence :reference do |n|
    RandomTokenGenerator.call(uniq_id: n, type: :merchant_transaction_reference_token).result
  end

  factory :merchant_transaction do
    reference
    sequence(:description) {  Faker::Commerce.product_name }
    sequence(:amount) {  Faker::Commerce.price }
  end
end
