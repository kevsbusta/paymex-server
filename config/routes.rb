Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :merchants, only: [:index, :update, :show, :create]
  resources :merchant_transactions, only: [:create, :show]
  resources :online_merchant_payments, only: [:create]
  resources :paymex_keys do
    member do
      post :deny
      post :cancel
      post :approve
    end
  end
  resources :pending_paymex_keys, only: [:index]
  resources :physical_merchant_payments, only: [:create]
  resources :request_paymex_keys, only: [:create]
  resources :send_paymex_keys, only: [:create]
  resources :checkouts, only: [:new]
  resources :received_paymex_keys, only: [:index]
  resources :sent_paymex_keys, only: [:index]
  resources :unconsumed_paymex_keys, only: [:index]
  resources :users, only: [:show, :create]

  resources :user_authentication do
    collection do
      post :authenticate
    end
  end

  resources :merchant_authentication do
    collection do
      post :authenticate
    end
  end
end
