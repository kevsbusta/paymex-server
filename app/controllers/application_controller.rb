class ApplicationController < ActionController::API

  attr_reader :current_user
  include ExceptionHandler
  include ClaimHandler

end
