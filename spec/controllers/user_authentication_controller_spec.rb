require 'rails_helper'

RSpec.describe UserAuthenticationController, type: :controller do
	let!(:hashed_password) { Digest::MD5.hexdigest('foobar') }
  let!(:user) { create(:user, mobile: "639231234567", email: 'test@domain.com', password: hashed_password) }


 	subject(:context) do
  	post :authenticate, params:
    	{
    		mobile: mobile,
    		password: password
    	}
	end

  describe '.authenticate' do

    context 'when user is authorized' do
    	let!(:mobile) { "639231234567" }
    	let!(:password) { hashed_password }

      it 'returns status 200' do
        expect(context).to have_http_status(200)
      end

      it 'returns a valid token in the response' do
        actual = JSON.parse(context.body)
        expect(actual['auth_token']).to_not be_nil
      end

      it 'returns a valid token with claims of id' do
        res = JSON.parse(context.body)
        secret_key = Rails.application.secrets.secret_key_base
        actual = JWT.decode(res['auth_token'], secret_key).first

        expect(actual['user_id']).to eq(user.id)
      end
    end

    context 'when user is NOT authorized' do
    	context 'when user has an invalid mobile' do
        let!(:mobile) { "639231234569" }
				let(:password) { 'IamNotCorrect' }

	      it 'returns status 401' do
	        expect(context).to have_http_status(401)
	      end
    	end

      context 'whene password is incorrect' do
        let!(:mobile) { "639231234567" }
        let(:password) { 'IamNotCorrect' }

	 			it 'returns status 401' do
        	expect(context).to have_http_status(401)
      	end
      end
    end
  end
end
