class PaymexKey < ApplicationRecord

  #TODO validate receiver id and sender id
  validates :key, :granted_amount, :expires_at, :sender_id, :receiver_id, presence: true

  before_validation :generate_key
  before_validation :generate_expiry_date

  belongs_to :sender, class_name: User, foreign_key: :sender_id
  belongs_to :receiver, class_name: User, foreign_key: :receiver_id


  def readonly!
    super
  end

  def cancel!
    raise ActionNotAllowed unless status.in?(["approved", "for_approval", "pending"])
    self.update(status: "cancelled")
  end

  def deny!
    raise ActionNotAllowed unless status.in?(["for_approval", "pending"])
    self.update(status: "denied")
  end

  def approve!
    raise ActionNotAllowed unless status.in?(["for_approval", "pending"])
    self.update(status: "approved")
  end

  def approved?
    true if status == 'approved'
  end

  protected

  def generate_key
    return unless self.key.nil?
    self.key = loop do
      key = rand.to_s[2..17]
      break key unless PaymexKey.exists?(key: key)
    end
  end

  def generate_expiry_date
    return unless self.expires_at.nil?
    self.expires_at = Time.now.to_date + 7.days
  end
end
