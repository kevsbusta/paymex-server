class CreateTransactionKeys < ActiveRecord::Migration[5.0]
  def change
    create_table :transaction_keys do |t|
      t.string :description
      t.string :key
      t.string :currency
      t.decimal :granted_amount
      t.decimal :consumed_amount
      t.integer :receiver_id
      t.integer :sender_id
      t.string :status, default: 'pending'
      t.datetime :status_last_updated_at
      t.datetime :consumed_at
      t.datetime :expires_at
      t.timestamps
    end
  end
end


