class SendPaymexKeyHandler
  prepend SimpleCommand

  attr_reader :send_key_params, :user_id

  def initialize(send_key_params: nil, user_id: nil)
    @send_key_params = send_key_params
    @user_id = user_id

    raise InvalidNilParameter, instance_values if instance_values.values.include?('') ||  instance_values.values.include?(nil)
  end

  def call
      PaymexKey.create!(
          sender_id: user_id,
          receiver_id: receiver_id,
          status: 'approved',
          granted_amount: send_key_params['granted_amount'],
          description: send_key_params['description']
      )
  end

  def receiver_id
    receiver = User.find_by(mobile: send_key_params[:mobile])
    return receiver.id if receiver.present?
    nil
  end
end