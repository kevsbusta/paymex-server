Braintree::Configuration.environment = Rails.configuration.braintree['environment']
Braintree::Configuration.merchant_id = Rails.configuration.braintree['merchant_id']
Braintree::Configuration.public_key  = Rails.configuration.braintree['public_key']
Braintree::Configuration.private_key = Rails.configuration.braintree['private_key']
