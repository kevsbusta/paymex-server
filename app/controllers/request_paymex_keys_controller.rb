class RequestPaymexKeysController < UserAuthApiController

  def create
    command = RequestPaymexKeyHandler.call(request_key_params: request_key_params, user_id: user_id)
    render json: command.result, status: :ok if command.success?
  end

  private

  def request_key_params
    params.permit(:mobile, :first_name, :last_name, :description, :granted_amount)
  end
end
