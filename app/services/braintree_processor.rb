class BraintreeProcessor
  prepend SimpleCommand

  attr_reader :paymex_key, :merchant_transaction, :sender

  def initialize(paymex_key: nil, merchant_transaction: nil)
    @paymex_key = paymex_key
    @merchant_transaction =  merchant_transaction
    @sender = paymex_key.sender

    raise InvalidNilParameter, instance_values if instance_values.values.include?(nil)
  end

  def call
    if sender.braintree_id.nil?
      errors.add :braintree_sender, 'Sender is not eligible for payment'
      return
    end

    nonce = payment_method_nonce
    result = Braintree::Transaction.sale(
      amount: merchant_transaction.amount,
      payment_method_nonce: nonce,
      :customer_id => @sender.braintree_id,
      :options => {
        :store_in_vault_on_success => true
      }
    )

    if result.instance_of? Braintree::ErrorResult
      errors.add :braintree_transaction, 'Transaction failed'
    end

    nil
  end

  private

  # To be change for the sake of demo
  def default_payment_method
    @payment_method ||= braintree_customer.payment_methods.first
  end

  def braintree_customer
    @braintree_customer ||= Braintree::Customer.find(sender.braintree_id)
  end

  def payment_method_nonce
    BraintreeNonceGenerator.
      call(payment_method_token: default_payment_method.token).
        result.
        payment_method_nonce.
        nonce
  end
end
