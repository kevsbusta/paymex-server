require 'rails_helper'

RSpec.describe OnlineMerchantPaymentsController, type: :controller do
  describe 'POST create' do
    let(:merchant) { create(:merchant, id: 1) }


    subject(:context) do
      post :create,
           params: {
               reference: merchant_transaction.reference,
               key: paymex_key.key,
               expiry_date: expiry_date.strftime('%Y/%m/%d')
           }
    end

    context 'when create is successful' do
      before do
        sender = paymex_key.sender
        sender.update(braintree_id: 860513836)
      end

      let(:expiry_date) { (Time.now+7.day).to_date }
      let(:merchant_transaction) do
        create(:merchant_transaction,
               merchant_id: 1,
               amount: 300)
      end
      let(:paymex_key) { create(:approved_paymex_key, granted_amount: 400 ) }

      it 'returns payment reference' do
        actual_result = context.body
        expect(actual_result).to eq merchant_transaction.reference
      end

      pending "marks the payment transaction as pai #{__FILE__}"

      pending "marks the payment key as consumed #{__FILE__}"
    end

    context 'when create is not successful' do
      context 'when expiry_date and key does not match' do
        let(:expiry_date) { (Time.now+1.day).to_date }
        let(:merchant_transaction) do
          create(:merchant_transaction,
                 merchant_id: 1,
                 amount: 300)
        end
        let(:paymex_key) { create(:approved_paymex_key, expires_at: (expiry_date+2.days).strftime('%Y/%m/%d'), granted_amount: 400 ) }


        it "returns error message" do
          actual_result = JSON.parse(context.body)
          expected_result =  {"message"=>"Paymex key invalid"}
          expect(actual_result).to eq(expected_result)
        end
      end

      context 'when key has already expired' do
        let(:expiry_date) { (Time.now-1.day).to_date }
        let(:merchant_transaction) do
          create(:merchant_transaction,
                 merchant_id: 1,
                 amount: 300)
        end
        let(:paymex_key) { create(:approved_paymex_key, expires_at: expiry_date.strftime('%Y/%m/%d'), granted_amount: 400 ) }

        it "returns error message" do
          actual_result = JSON.parse(context.body)
          expected_result = {"message"=>"Paymex key has expired"}
          expect(actual_result).to eq(expected_result)
        end
      end

      context 'when payment key is not yet approved' do
        let(:expiry_date) { (Time.now+1.day).to_date }
        let(:merchant_transaction) do
          create(:merchant_transaction,
                 merchant_id: 1,
                 amount: 300)
        end
        let(:paymex_key) { create(:pending_paymex_key, expires_at: expiry_date.strftime('%Y/%m/%d'), granted_amount: 400 ) }

        it "retruns error message" do
          actual_result = JSON.parse(context.body)
          expected_result =  {"message"=>"Paymex key invalid"}
          expect(actual_result).to eq(expected_result)
        end
      end

      context 'when sender has no braintree account' do
        before do
          sender = paymex_key.sender
          sender.update(braintree_id: nil)
        end

        let(:expiry_date) { (Time.now+7.day).to_date }
        let(:merchant_transaction) do
          create(:merchant_transaction,
                 merchant_id: 1,
                 amount: 300)
        end
        let(:paymex_key) { create(:approved_paymex_key, granted_amount: 400 ) }

        it 'returns precondition failred response' do
          expect(context).to have_http_status(:precondition_failed)
        end
      end
    end
  end
end
