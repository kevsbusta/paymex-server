class PaymexKeyFetcher

  prepend SimpleCommand

  attr_reader :key, :expiry_date

  def initialize(key: nil, expiry_date: nil)
    @key = key
    @expiry_date =  Date.parse(expiry_date, '%Y-%m-%d')

    raise InvalidNilParameter, instance_values if instance_values.values.include?(nil)
    raise PaymexKeyExpired if Time.now.to_date > @expiry_date
  end

  def call
    @paymex_key = retrieve_paymex_key
    raise PaymexKeyInvalid unless @paymex_key
    if @paymex_key.approved?
      return @paymex_key
    else
      raise PaymexKeyNotApproved
    end
  end

  def retrieve_paymex_key
    PaymexKey.find_by(key: key, expires_at: expiry_date)
  end
end