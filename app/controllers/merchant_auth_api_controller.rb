class MerchantAuthApiController < ApplicationController
	before_action :authenticate_merchant

  def authenticate_merchant
    command = AuthorizeMerchantApiRequest.call(request.headers)

    if command.success?
			@current_user = command.result
    else
      render json: { message: "Merchant Not Authorized" }, status: :unauthorized
    end

  end
end
