class RandomTokenGenerator
  prepend SimpleCommand

  attr_reader :uniq_id, :length, :type

  def initialize(uniq_id: nil, type: :merchant_code_token)
    @uniq_id = uniq_id
    @length = length
    @type = type
  end

  def call
    self.send(type)
  end

  def merchant_code_token
    "MC-#{SecureRandom.hex(2)}-#{uniq_id}"
  end

  def merchant_transaction_reference_token
    "MR-#{SecureRandom.hex(4)}-#{uniq_id}"
  end
end