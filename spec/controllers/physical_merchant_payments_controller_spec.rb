require 'rails_helper'

RSpec.describe PhysicalMerchantPaymentsController, type: :controller do

  describe 'POST create' do
    let(:merchant) { create(:merchant, id: 1) }

    subject(:context) do
      post :create,
           params: {
               description: description,
               key: paymex_key.key,
               expiry_date: expiry_date,
               amount: amount
           }
    end

    context 'when create is successful' do
      let(:expiry_date) { (Time.now+7.day).to_date }
      let(:description) { Faker::Commerce.product_name }
      let(:amount) { 300  }
      let(:paymex_key) { create(:approved_paymex_key, granted_amount: 400 ) }

      it 'creates merchant transaction' do
        expect{ context }.to change(MerchantTransaction, :count).by(1)
      end

      it 'returns payment reference' do
        actual_result = context.body
        expect(actual_result).to eq MerchantTransaction.last.reference
      end

        pending "marks the payment transaction as paid  #{__FILE__}"
        pending "marks the payment key as consumed #{__FILE__}"
    end

    context 'when create is not successful' do
      context 'when expiry_date and key does not match' do
        let(:expiry_date) { (Time.now+3.day).to_date }
        let(:description) { Faker::Commerce.product_name }
        let(:amount) { 300  }
        let(:paymex_key) { create(:approved_paymex_key, granted_amount: 400 ) }

        it "returns error message" do
          actual_result = JSON.parse(context.body)
          expected_result =  {"message"=>"Paymex key invalid"}
          expect(actual_result).to eq(expected_result)
        end
      end

      context 'when key has already expired' do
        let(:expiry_date) { (Time.now-1.day).to_date }
        let(:description) { Faker::Commerce.product_name }
        let(:amount) { 300  }
        let(:paymex_key) { create(:approved_paymex_key, expires_at: expiry_date.strftime('%Y/%m/%d'), granted_amount: 400 ) }


        it "returns error message" do
          actual_result = JSON.parse(context.body)
          expected_result = {"message"=>"Paymex key has expired"}
          expect(actual_result).to eq(expected_result)
        end
      end

      context 'when payment key is not yet approved' do
        let(:expiry_date) { (Time.now+1.day).to_date }
        let(:description) { Faker::Commerce.product_name }
        let(:amount) { 300  }
        let(:paymex_key) { create(:pending_paymex_key, expires_at: expiry_date.strftime('%Y/%m/%d'), granted_amount: 400 ) }


        it "returns error message" do
          actual_result = JSON.parse(context.body)
          expected_result =  {"message"=>"Paymex key invalid"}
          expect(actual_result).to eq(expected_result)
        end
      end

      context 'when parameters contain empty value' do
        let(:expiry_date) { (Time.now+1.day).to_date }
        let(:description) { Faker::Commerce.product_name }
        let(:amount) { 300  }
        let(:paymex_key) { create(:pending_paymex_key, expires_at: expiry_date.strftime('%Y/%m/%d'), granted_amount: 400 ) }

        it "returns error message" do
          actual_result = JSON.parse(context.body)
          expected_result =  {"message"=>"Paymex key invalid"}
          expect(actual_result).to eq(expected_result)
        end
      end
    end
  end


end
