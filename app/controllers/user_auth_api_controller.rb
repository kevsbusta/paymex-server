class UserAuthApiController < ApplicationController
	before_action :authenticate_user

  def authenticate_user
    command = AuthorizeUserApiRequest.call(request.headers)

    if command.success?
			@current_user = command.result
    else
      render json: { message: "User Not Authorized" }, status: :unauthorized
    end
  end
end
