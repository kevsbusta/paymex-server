FactoryBot.define do
  sequence :code do |n|
    RandomTokenGenerator.call(uniq_id: n, type: :merchant_code_token).result
  end

  factory :merchant do
    sequence(:name) { Faker::Company.name }
    code
  end
end
