class PaymentProcessor

  prepend SimpleCommand

  attr_reader :paymex_key, :merchant_transaction

  def initialize(paymex_key: nil, merchant_transaction: nil)

    @paymex_key = paymex_key
    @merchant_transaction =  merchant_transaction


    raise AmountInsufficient if @paymex_key.granted_amount < @merchant_transaction.amount
    raise InvalidNilParameter, instance_values if instance_values.values.include?(nil)
  end

  def call
    MerchantTransaction.transaction do
      @merchant_transaction.update(
        paymex_key_id: paymex_key.id,
        status: status,
        status_last_updated_at: Time.now,
        paid_at: Time.now
      )

      #TODO update key to consumed
    end
  end

  def status
    'paid'
  end

end